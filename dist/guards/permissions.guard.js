"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionsRequired = void 0;
const common_1 = require("@nestjs/common");
// If some endpoint have more than one permission like:
// @PermissionsRequired('permission:one', 'permission:two')
// that means the user needs to have one of them to access the resource
const PermissionsRequired = (...permissions) => (0, common_1.SetMetadata)('permissionsRequired', permissions);
exports.PermissionsRequired = PermissionsRequired;
//# sourceMappingURL=permissions.guard.js.map