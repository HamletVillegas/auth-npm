"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthGuard = exports.Public = void 0;
/* eslint-disable import/no-extraneous-dependencies */
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const passport_jwt_1 = require("passport-jwt");
const auth_service_1 = require("../auth.service");
const IS_PUBLIC_KEY = 'isPublic';
const Public = () => (0, common_1.SetMetadata)(IS_PUBLIC_KEY, true);
exports.Public = Public;
let AuthGuard = class AuthGuard {
    // private user: any;
    constructor(reflector, authService) {
        this.reflector = reflector;
        this.authService = authService;
    }
    async canActivate(context) {
        const isPublic = this.reflector.getAllAndOverride(IS_PUBLIC_KEY, [context.getHandler(), context.getClass()]) || false;
        if (isPublic)
            return true;
        const permissionsRequired = this.reflector.get('permissionsRequired', context.getHandler());
        const request = 'switchToHttp' in context ? context.switchToHttp().getRequest() : context.args[0];
        const token = passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken()(request);
        if (!token)
            throw new common_1.UnauthorizedException('Unauthorized request');
        const { data } = await this.authService.validateJwt(token, permissionsRequired);
        // Now we can use @AuthUser() decorator on each http request
        request.user = data;
        return true;
    }
};
AuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [core_1.Reflector, auth_service_1.AuthService])
], AuthGuard);
exports.AuthGuard = AuthGuard;
//# sourceMappingURL=auth.guard.js.map