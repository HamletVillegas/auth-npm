import { ClientProxy } from '@nestjs/microservices';
export declare class AuthService {
    private authService;
    constructor(authService: ClientProxy);
    validateJwt(token: string, permissionsRequired?: string[]): Promise<any>;
}
//# sourceMappingURL=auth.service.d.ts.map