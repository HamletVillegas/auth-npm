import { ForbiddenException, HttpStatus, Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class AuthService {
  constructor(@Inject('AUTH_SERVICE') private authService: ClientProxy) {}

  async validateJwt(token: string, permissionsRequired?: string[]) {
    try {
      const data = this.authService.send({ cmd: 'authenticate' }, { token, permissionsRequired });

      return await lastValueFrom(data);
    } catch (error:any) {
      if (error.code === HttpStatus.FORBIDDEN) throw new ForbiddenException(error);
      else throw new UnauthorizedException(error);
    }
  }
}
