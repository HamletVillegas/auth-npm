interface Settings {
  language: string;
  unitMass: string;
  timezone: string;
}

export interface AuthUser {
  producerId: string;
  userId: string;
  producer: string;
  email: string;
  name: string;
  lastname: string;
  settings: Settings;
  iat: number;
  exp: number;
  rolName: string;
}
