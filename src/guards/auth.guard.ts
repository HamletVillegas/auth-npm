/* eslint-disable import/no-extraneous-dependencies */
import { Injectable, SetMetadata, ExecutionContext, UnauthorizedException, CanActivate } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { ExtractJwt } from 'passport-jwt';
import { AuthService } from '../auth.service';

const IS_PUBLIC_KEY = 'isPublic';
export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);

@Injectable()
export class AuthGuard implements CanActivate {
  // private user: any;

  constructor(private reflector: Reflector, private authService: AuthService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic =
      this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [context.getHandler(), context.getClass()]) || false;

    if (isPublic) return true;

    const permissionsRequired = this.reflector.get<string[]>('permissionsRequired', context.getHandler());

    const request = 'switchToHttp' in context ? context.switchToHttp().getRequest<Request>() : (context as any).args[0];
    const token = ExtractJwt.fromAuthHeaderAsBearerToken()(request);

    if (!token) throw new UnauthorizedException('Unauthorized request');

    const { data } = await this.authService.validateJwt(token, permissionsRequired);

    // Now we can use @AuthUser() decorator on each http request
    request.user = data;

    return true;
  }

  // handleRequest(err) {
  //   if (err) return err;

  //   if (this.user === null) return new UnauthorizedException();

  //   return this.user;
  // }
}
