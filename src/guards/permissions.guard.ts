import { SetMetadata } from '@nestjs/common';

// If some endpoint have more than one permission like:
// @PermissionsRequired('permission:one', 'permission:two')
// that means the user needs to have one of them to access the resource
export const PermissionsRequired = (...permissions: string[]) => SetMetadata('permissionsRequired', permissions);
