// jest.d.ts

declare namespace jest {
    interface Matchers<R> {
      toBeDivisibleBy(divisor: number): CustomMatcherResult;
    }
  }
  