import { SetMetadata } from '@nestjs/common';
import { PermissionsRequired } from '../../src/guards/permissions.guard';

describe('PermissionsRequired', () => {
  it('should return a SetMetadata instance with the expected values', () => {
    const permissions = ['permission:one', 'permission:two'];
    const result = PermissionsRequired(...permissions);

    expect(result).toBeDefined();
    expect(result['KEY']).toEqual('permissionsRequired');
  });
});
