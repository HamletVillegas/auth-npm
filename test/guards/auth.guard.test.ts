import { Reflector } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../../src/auth.service';
import { AuthGuard, Public } from '../../src/guards/auth.guard';
import { of } from 'rxjs';
import { ExtractJwt } from 'passport-jwt/lib';

class MockAuthService {
  validateJwt = jest.fn();
}

class MockReflector {
  getAllAndOverride = jest.fn();
  get = jest.fn();
}

describe('AuthGuard', () => {

  describe('AuthGuard ', () =>{
    let authGuard: AuthGuard;
    let mockAuthService: MockAuthService;
    let mockReflector: MockReflector;
    let mockExecutionContext: any;
    let mockPermissionsRequired: string[];
    let mockContextArgs: any;
    beforeEach(async () => {
      mockAuthService = new MockAuthService();
      mockReflector = new MockReflector();
      mockExecutionContext = {
        getHandler: jest.fn(),
        getClass: jest.fn(),
        switchToHttp: jest.fn().mockReturnThis(),
        getRequest: jest.fn(),
      };
      mockContextArgs = {
        getHandler: jest.fn(),
        getClass: jest.fn(),
        getRequest: jest.fn(),
        args: [
          {
              headers: {
                  'authorization': 'Bearer XXXXXXXXXXXX'
              }
          }
      ]
      };
      mockPermissionsRequired = ['ADMIN'];
  
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          AuthGuard,
          {
            provide: AuthService,
            useValue: mockAuthService,
          },
          {
            provide: Reflector,
            useValue: mockReflector,
          }
        ],
      }).compile();
  
      authGuard = module.get<AuthGuard>(AuthGuard);
    });
  
    it('should allow access to public routes', async () => {
      mockReflector.getAllAndOverride.mockImplementation(() =>true);
      mockExecutionContext.getRequest.mockReturnValueOnce({});
      const canActivateResult = await authGuard.canActivate(mockExecutionContext);
  
      expect(canActivateResult).toBe(true);
      expect(mockAuthService.validateJwt).not.toHaveBeenCalled();
      expect(mockExecutionContext.getRequest).not.toHaveBeenCalled();
    }); 
  
    it('should allow access to authorized users', async () => {
      mockReflector.getAllAndOverride.mockImplementation(() =>false);
      mockReflector.get.mockReturnValueOnce(mockPermissionsRequired);
      mockExecutionContext.getRequest.mockReturnValueOnce({
        headers: {
          authorization: 'Bearer validToken',
        },
      });
      mockAuthService.validateJwt.mockReturnValueOnce(of({ data: { id: 1 }, permissions: mockPermissionsRequired }));
  
      const canActivateResult = await authGuard.canActivate(mockExecutionContext);
  
      expect(canActivateResult).toBe(true);
      expect(mockAuthService.validateJwt).toHaveBeenCalledWith('validToken', mockPermissionsRequired);
      expect(mockExecutionContext.getRequest).toHaveBeenCalledTimes(1);
      expect(mockExecutionContext.getRequest).toHaveBeenCalledWith();
    });
  
    it('should throw UnauthorizedException if get request fails ', async () => {
      mockReflector.getAllAndOverride.mockImplementation(() =>false);
      mockReflector.get.mockReturnValueOnce(mockPermissionsRequired);
      mockExecutionContext.getRequest.mockReturnValueOnce({headers: {}});
      mockAuthService.validateJwt.mockReturnValueOnce(of(null));
      await expect(authGuard.canActivate(mockExecutionContext)).rejects.toThrowError(UnauthorizedException);
    });
  
    it('should get true when token is valid', async () => {
      mockReflector.getAllAndOverride.mockImplementation(() =>false);
      mockReflector.get.mockReturnValueOnce(mockPermissionsRequired);
      mockAuthService.validateJwt.mockImplementationOnce(async() => ({data:{}}));
     
      const response = await authGuard.canActivate(mockContextArgs);
      expect(response).toBeTruthy()
      
    });
  })
  describe('Public Decorate', () =>{
    it('should be defined', () =>{
      expect(Public()).toBeDefined();
    })

  })

});
