import {AuthService} from '../src/auth.service'
import {Test,TestingModule} from '@nestjs/testing'
import { of } from 'rxjs';
import { ClientProxy } from '@nestjs/microservices';
import {AuthUser} from '../src/interfaces/auth-user.interface'
import { ForbiddenException, HttpStatus, UnauthorizedException } from '@nestjs/common';

class MockedClientProxy {
  send() {
    return jest.fn();
  }
}


describe('Auth service Test',() =>{
  let authService: AuthService;
  let authClientService: ClientProxy;

  beforeEach(async () =>{
    const module: TestingModule = await Test.createTestingModule({
      providers:[
        AuthService,
        {provide:'AUTH_SERVICE',useClass:MockedClientProxy}
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    authClientService = module.get<ClientProxy>('AUTH_SERVICE');
  })

  describe('ValidateJWT', () =>{

    it('should send messages only used token',async () =>{
      jest.spyOn(authClientService,'send').mockImplementationOnce(()=> of(null));
      authService.validateJwt('Token_valid');
      expect(authClientService.send).toHaveBeenCalledTimes(1);
      expect(authClientService.send).toHaveBeenCalledWith({"cmd": "authenticate"}, {"permissionsRequired": undefined, "token": "Token_valid"});
    })

    it('should send messages with token and permissions',async () =>{
      jest.spyOn(authClientService,'send').mockImplementationOnce(()=> of(null));
      authService.validateJwt('Token_valid',['list_products']);
      expect(authClientService.send).toHaveBeenCalledTimes(1);
      expect(authClientService.send).toHaveBeenCalledWith({"cmd": "authenticate"}, {"permissionsRequired": ['list_products'], "token": "Token_valid"});
    })

    it('should throw ForbiddenException if token is invalid',async () =>{
      jest.spyOn(authClientService,'send').mockImplementationOnce(()=> of(Promise.reject({ code: HttpStatus.FORBIDDEN })));
      await expect(authService.validateJwt('invalid_token')).rejects.toThrowError(ForbiddenException);
      expect(authClientService.send).toHaveBeenCalledWith({ cmd: 'authenticate' }, { token: 'invalid_token', permissionsRequired: undefined });
    })

    it('should throw UnauthorizedException if authentication fails for other reasons',async () =>{
      jest.spyOn(authClientService,'send').mockImplementationOnce(()=> of(Promise.reject({ code: HttpStatus.UNAUTHORIZED })));
      await expect(authService.validateJwt('invalid_token')).rejects.toThrowError(UnauthorizedException);
      expect(authClientService.send).toHaveBeenCalledWith({ cmd: 'authenticate' }, { token: 'invalid_token', permissionsRequired: undefined });
    })

    

  })
})
