import { createParamDecorator as createParamDecoratorReal, ExecutionContext } from '@nestjs/common';
const mockUser = { id: 1, name: 'Test User' };


const mockExecutionContext = {
  switchToHttp:jest.fn().mockReturnThis(),
  getRequest:jest.fn().mockReturnThis(),
  user:mockUser

}

export const createParamDecoratorMock = jest.fn(
  (callback: (data: any, ctx: ExecutionContext) => any) => {
    return jest.fn((data: any, ctx: ExecutionContext) => {
      return callback(data, ctx);
    });
  },
);

jest.mock('@nestjs/common', () => ({
  ...jest.requireActual('@nestjs/common'),
  createParamDecorator: createParamDecoratorMock,
}));

import { AuthenticatedUser } from '../../src/decorators/authenticated-user.decorator';


describe('AuthUser', () => {
  it('should return the authenticated user from the request', () => {
    const result = AuthenticatedUser(null, mockExecutionContext);
    expect(result).toBe(mockUser);
  });

});
